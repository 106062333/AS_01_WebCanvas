function Do(canvas)
{
    // Store an odler version of canvas
    if(Data.undoIdx != 0)
    {
        Data.undoList.splice(0, Data.undoIdx);
        Data.undoIdx = 0;
    }
    if(Data.undoList.length >= 10)
    {
        Data.undoList.splice(9);
    }
    Data.undoList.splice(0, 0, {id: canvas.id, data:canvas.toDataURL()});
    undoIdx = 0;
    ToolUpdate();
}

function Undo()
{
    if(Data.undoIdx >= Data.undoList.length) return;
    // undo the operation at undoIdx
    // after replacing it with current status
    var pic = new Image();
    pic.src = Data.undoList[Data.undoIdx].data;
    pic.onload = function(){
        // pic is the BEFORE state
        cv = document.getElementById(Data.undoList[Data.undoIdx].id);
        // cv is the CURRENT state
        Data.undoList[Data.undoIdx] = {id: cv.id, data: cv.toDataURL()};
        ctx = cv.getContext('2d');
        ctx.globalCompositeOperation = 'copy';
        ctx.globalAlpha = 1;
        ctx.drawImage(pic, 0, 0);
        // Prepare for redo
        Data.undoIdx++;
        ToolUpdate();
        ctx.globalCompositeOperation = 'source-over';
    }
}

function Redo()
{
    if(Data.undoIdx == 0) return;
    var pic = new Image();
    pic.src = Data.undoList[Data.undoIdx-1].data;
    pic.onload = function(){
        // pic is the NEW state
        cv = document.getElementById(Data.undoList[Data.undoIdx-1].id);
        // cv is the CURRENT state
        Data.undoList[Data.undoIdx-1] = {id: cv.id, data: cv.toDataURL()};
        ctx = cv.getContext('2d');
        ctx.globalCompositeOperation = 'copy';
        ctx.globalAlpha = 1;
        ctx.drawImage(pic, 0, 0);
        // Prepare for undo
        Data.undoIdx--;
        ToolUpdate();
        ctx.globalCompositeOperation = 'source-over';
    }
}

function CanUndo()
{
    return Data.undoIdx < Data.undoList.length;
}

function CanRedo()
{
    return Data.undoIdx != 0;
}