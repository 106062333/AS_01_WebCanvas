ts_ids = ['TS_Brush', 'TS_Eraser', 'TS_Line', 'TS_Circle', 'TS_Square', 'TS_Triangle', 'TS_Text', 'TS_Scale'];

function TS_ChangeTo(tool){
    for(i = 0; i < ts_ids.length; i++) document.getElementById(ts_ids[i]).style.display = 'none';
    document.getElementById(tool).style.display = 'block';
}

function TS_Brush_Updated()
{
    size = document.getElementById("TS_Brush_Size");
    op = document.getElementById("TS_Brush_Opacity");
    opVal = document.getElementById("TS_Brush_Opacity_Val");
    sizeVal = document.getElementById("TS_Brush_Size_Val");
    opVal.innerHTML = op.value;
    sizeVal.innerHTML = size.value;

    Data.TS.Brush.Size = parseInt(size.value);
    Data.TS.Brush.Opacity = parseInt(op.value);
}

function TS_Eraser_Updated()
{
    size = document.getElementById("TS_Eraser_Size");
    sizeVal = document.getElementById("TS_Eraser_Size_Val");
    sizeVal.innerHTML = size.value;

    Data.TS.Eraser.Size = parseInt(size.value);
}

function TS_Shape_Updated()
{
    shapes = ['Line', 'Circle', 'Square', 'Triangle'];
    for(i = 0; i < shapes.length; i++)
    {
        width = document.getElementById(`TS_${shapes[i]}_Width`);
        op = document.getElementById(`TS_${shapes[i]}_Opacity`);
        widthVal = document.getElementById(`TS_${shapes[i]}_Width_Val`);
        opVal = document.getElementById(`TS_${shapes[i]}_Opacity_Val`);
        filled = document.getElementById(`TS_${shapes[i]}_Filled`);
        widthVal.innerHTML = width.value;
        opVal.innerHTML = op.value;
        eval(`Data.TS.${shapes[i]}.Width = ${parseInt(width.value)};`);
        eval(`Data.TS.${shapes[i]}.Opacity = ${parseInt(op.value)};`);
        if(i != 0) eval(`Data.TS.${shapes[i]}.Filled = ${filled.checked};`);
    }
}

function TS_Text_Updated()
{
    fontname = document.getElementById('TS_Text_FontName');
    size = document.getElementById('TS_Text_Size');
    sizeVal = document.getElementById('TS_Text_Size_Val');
    op = document.getElementById('TS_Text_Opacity');
    opVal = document.getElementById('TS_Text_Opacity_Val');
    bold = document.getElementById('TS_Text_Bold');
    italic = document.getElementById('TS_Text_Italic');

    Data.TS.Text.Font = fontname.value;
    Data.TS.Text.Size = parseInt(size.value);
    Data.TS.Text.Opacity = parseInt(op.value);
    Data.TS.Text.Bold = bold.checked;
    Data.TS.Text.Italic = italic.checked;

    sizeVal.innerHTML = size.value;
    opVal.innerHTML = op.value;
}
