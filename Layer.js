function NewLayer()
{
    Data.layersCreated++;
    nc = document.createElement('canvas');
    nc.className = 'fit-width';
    nc.width = 640;
    nc.height = 480;
    nc.style.zIndex = parseInt(Data.canvas.style.zIndex)+2;
    nc.id = `layer${Data.layersCreated}`;
    document.getElementById('canvasHolder').appendChild(nc);

    // Adjust the array
    // z-index
    for(i = Data.selectedLayer+1; i < Data.layers.length; i++)
    {
        Data.layers[i].style.zIndex = Data.canvas.style.zIndex + 2 * (i - Data.selectedLayer + 1);
    }
    // insertion
    Data.layers.splice(Data.selectedLayer+1, 0, nc);
    // Adjust the layer panel
    LayerPanelReconfigure();
    LayerSelected(document.getElementById(`dl_${Data.selectedLayer+1}`));
}

function LayerPanelReconfigure()
{
    var al = document.getElementById("AllLayer");
    al.innerHTML = "";
    for(i = Data.layers.length-1; i >= 1; i--)
    {
        al.innerHTML += `<div style="border-left:#d6dde4 solid 5px" class="layer  non-selectable" id="dl_${i}" onclick="LayerSelected(this)" onmouseover="LayerMouseEnter(this)" onmouseout="LayerMouseLeave(this)"> 
            <div>
                <table>
                    <tr>
                        <td colspan="1" rowspan="2" style="width: 70%">${Data.layers[i].id}</td>
                        <td rowspan="2"><img src="img/separator.png"/></td>
                        <td><img src="img/layer/moveup.png" class="layercontrol non-selectable" onclick="LayerMoveUp(${i})"` + (i < Data.layers.length-1 ? "" : " style='opacity: 0;'") + `/></td>
                        <td><img src="img/layer/collapse.png" class="layercontrol non-selectable" onclick="LayerCollapse(${i})"/></td>
                    </tr>
                    <tr>
                        <td><img src="img/layer/movedown.png" class="layercontrol non-selectable" onclick="LayerMoveDown(${i})"` + (i > 1 ? "" : " style='opacity: 0;'") + `/></td>
                        <td><img src="img/layer/discard.png" class="layercontrol non-selectable" onclick="LayerDelete(${i})"/></td>
                    </tr>
                </table>
            </div>
        </div>`;
    }
}

function LayerMouseEnter(obj)
{
    obj.style.borderLeft = "#819cab solid 5px";
}

function LayerMouseLeave(obj)
{
    number = parseInt(obj.id.substr(3));
    if(number == Data.selectedLayer)
    {
        obj.style.borderLeft = "#819cab solid 5px";
    }
        
    else
    {
        obj.style.borderLeft = "#d6dde4 solid 5px";
    }
        
}

function LayerSelected(obj)
{
    // Deselect: border, font, bg
    el = document.getElementById(`dl_${Data.selectedLayer}`);
    if(el != null)
    {
        el.style.borderLeft = "#d6dde4 solid 5px";
        el.style.fontWeight = "normal";
        el.style.backgroundColor = "#e4e7eb";
    }
    

    // Select: Number, Canvas, Border, font, bg
    Data.selectedLayer = parseInt(obj.id.substr(3));
    console.log(`selecting layer: ${Data.selectedLayer}`);
    Data.canvas = Data.layers[Data.selectedLayer];
    if(Data.canvas == null) return;

    Data.ctx = Data.canvas.getContext('2d');
    el = document.getElementById(`dl_${Data.selectedLayer}`);
    el.style.borderLeft = "#819cab solid 5px";
    el.style.fontWeight = "bold";
    el.style.backgroundColor = "#d6dde4";
}

function LayerMoveUp(number)
{
    //number = parseInt(obj.parentElement.parentElement.parentElement.parentElement.parentElement.id.substr(3));
    if(number == Data.layers.length - 1) return;
    console.log(`moving up ${number}`);
    temp = Data.layers[number];
    Data.layers[number] = Data.layers[number+1];
    Data.layers[number+1] = temp;

    Data.layers[number].style.zIndex = parseInt(Data.layers[number].style.zIndex) - 2;
    
    Data.layers[number+1].style.zIndex = parseInt(Data.layers[number+1].style.zIndex) + 2;

    console.log(`The moved layer now has zindex of ${Data.layers[number+1].style.zIndex}, beneath is ${Data.layers[number].style.zIndex}`);

    // The layer panel
    LayerPanelReconfigure();
    setTimeout("LayerSelected(document.getElementById(`dl_${number+1}`))", 10);
}

function LayerMoveDown(number)
{
    //number = parseInt(obj.parentElement.parentElement.parentElement.parentElement.parentElement.id.substr(3));
    if(number <= 1) return;

    temp = Data.layers[number];
    Data.layers[number] = Data.layers[number-1];
    Data.layers[number-1] = temp;


    Data.layers[number].style.zIndex = parseInt(Data.layers[number].style.zIndex) + 2;
    
    Data.layers[number-1].style.zIndex = parseInt(Data.layers[number-1].style.zIndex) - 2;
    console.log(`The moved layer now has zindex of ${Data.layers[number-1].style.zIndex}, above is ${Data.layers[number].style.zIndex}`);
    // The layer panel
    LayerPanelReconfigure();
    setTimeout("LayerSelected(document.getElementById(`dl_${number-1}`))", 10);
}

function LayerCollapse(number, ignore=false)
{
    if(!ignore && !confirm('Are you sure you want to collapse this layer? This cannot be undone.')) return;
    
    // Copy number onto number-1, and then delete number
    ctx = Data.layers[number-1].getContext('2d');
    ctx.globalCompositeOperation = 'source-over';
    ctx.globalAlpha = 1;
    ctx.drawImage(Data.layers[number], 0, 0);

    LayerDelete(number, true);
}

function LayerDelete(number, ignore = false)
{
    if(!ignore && !confirm('Are you sure you want to delete this layer? This cannot be undone.')) return;
    Data.undoIdx = 0;
    if(Data.undoList.length > 0) Data.undoList.splice(0);
    ToolUpdate();
    //number = parseInt(obj.id.substr(3));
    for(i = number+1; i < Data.layers.length; i++)
    {
        Data.layers[i].style.zIndex = parseInt(Data.layers[i].style.zIndex) - 2;
    }
    toDel = Data.layers[number];
    Data.layers.splice(number, 1);
    toDel.remove();
    delete toDel;
    LayerPanelReconfigure();
    console.log(`will be selecting ${(Math.min(Data.layers.length-1, number))}`);
    setTimeout("LayerSelected(document.getElementById(`dl_${ (Math.min(Data.layers.length-1, number)) }`))", 10);
}

function CollapseAll()
{
    if(confirm('Are you sure to collapse all layers? This cannot be undone.'))
        while(Data.layers.length > 1)
        {
            LayerCollapse(1, true);
        }
}