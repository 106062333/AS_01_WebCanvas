# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
## Features
- Supports **Layer Edting**. Layers can be seperatedly designed, reordered, created, deleted, moved around, rotated, and scaled. You can also combine several layers together.
- All operation works only on the **Selected Layer**.
- Configurable brush (size, opacity, 3 shapes) and other tool options.
- An HSV Color Picker.
- Move/Rotate/Scale Tool.
- Upload/Download Images.
- Undo/Redo.
- Text input with font, size, opacity, bold, italic settings in the Tool Panel.
- Brush and Eraser have mouse icon as the drawn region.
- Other position-specific tools have a cross as cursor.
- Refresh button that refreshes the canvas.
- Inspiring quote that refreshes you.

## Tools
The tools are the left-top buttons in the page. \
They are used to change the contents on the canvas.\
Almost all tools have some configurable options; See the **Panel** part for reference.\
All tools only work on a **selected layer** at one time. See the **Features** part for reference.\
From left to right:

### Brush
<img src="img/tools/1_s.png" width=50 height=50/>\
The brush that can draw on the canvas using mouse as control. Options are available in **Tool** Panel: Size, Opacity, Brush Shape.

### Eraser
<img src="img/tools/2_s.png" width=50 height=50/>\
The eraser that clears whatever meets its path. **Transparent Images** are possible with this tool. Options are available in **Tool** Panel: Size, Brush Shape.

### Line
<img src="img/tools/3_s.png" width=50 height=50/>\
Line tool. Drag on the canvas to draw a line. Options are available in **Tool** Panel: Width, Opacity.
### Circle
<img src="img/tools/4_s.png" width=50 height=50/>\
Circle tool. Drag on the canvas to draw a circle. The firstly clicked position is the origin, the position where mouse is released is considered the radius from the origin. Options are available in **Tool** Panel: Width, Opacity, Filled.
### Square
<img src="img/tools/5_s.png" width=50 height=50/>\
Square tool. Drag on the canvas to draw a square. Clicked and released positions are the diagonal of the drawn square. Options are available in **Tool** Panel: Width, Opacity, Filled.
### Triangle
<img src="img/tools/6_s.png" width=50 height=50/>\
Triangle tool. Drag on the canvas to draw a triangle. The clicked position is the gravity center, the released one is one of the vertices. So the shaped adjusts accordingly. Options are available in **Tool** Panel: Width, Opacity, Filled.
### Text
<img src="img/tools/7_s.png" width=50 height=50/>\
The tool to put text on canvas. Use mouse to determine the position, and left-click to paste the text. Options are available in **Tool** Panel: Font, Size, Opacity, Bold, Italic, Text.
### Move
<img src="img/tools/8_s.png" width=50 height=50/>\
Move tool. Drag on the canvas to move a **layer** on canvas.
### Rotate
<img src="img/tools/9_s.png" width=50 height=50/>\
Rotation tool. Drag on the canvas to rotate a **layer** around the clicked position. The rotation angle is determined by the distance of released position with the clicked position - the farther the distance, the larger the angle.
### Scale
<img src="img/tools/10_s.png" width=50 height=50/>\
Scale tool. Drag on the canvas to scale a **layer**. The scaling amount is determined by the distance between clicked and released position.  Options are available in **Tool** Panel: Scale Proportionally.

## Functions
Functions are the top-right buttons.
From left to right:
### Upload
<img src="img/tools/12_s.png" width=50 height=50/>\
User can choose an image file to upload. A new **layer** will be created, and the uploaded image will be on the left-top corner of this new layer. Further operations can be done to **move, rotate,** or **resize** the layer, using the **Move, Rotate,** and **Scale** tool.
### Download
<img src="img/tools/13_s.png" width=50 height=50/>\
User can download the complete canvas, including **Transparency,** as an `.png` file.
### Undo/Redo
<img src="img/tools/14_s.png" width=50 height=50/>
<img src="img/tools/15_s.png" width=50 height=50/>

The Undo/Redo button. Numbers of undoable actions are limited (that is, you cannot undo all the way back after too many operations done). Note that:
- Creating new layers the adjusting layer ordering do not count as actions.
- Some actions are permanent, which means they cannot be undone, including **Deleteing a layer, Collapsing one/all layer(s)**

### Reset
<img src="img/tools/16_s.png" width=50 height=50/>\
The reset button. By pressing this, all the contents on all layers will be cleared. The created layers and their ordering will not be affected.
## Panels
Panels are the at the right part of the page.

### Layer
This is the settings of **Layers**. A **base layer** is always there; this cannot be changed (not without hacks lol). There are several operation that can be done with **Layers**.
#### Create New Layer
<img src="img/addlayer.png" width=50 height=50/>\
The **plus** button in the bottom of the layer panel lets you create a new layer. Layers are ordered; The ones at front block those behind.
#### Collapse All Layers
<img src="img/collapseall.png" width=50 height=50/>\
The **double arrow** in the bottom of the layer panel lets you combine all layers into the base layer. **This operation cannot be undone.**

As layers are created, there are also some useful functions to operate on layers.
#### Discard
<img src="img/layer/discard.png" width=50 height=50/>\
Delete the layer. **This operation cannot be undone.**
#### Collapse
<img src="img/layer/collapse.png" width=50 height=50/>\
Combine the layer with the layer beneath it. **This operation cannot be undone.**
#### Move Down
<img src="img/layer/movedown.png" width=50 height=50/>\
Reorder the layer, move it down by one depth.
#### Discard
<img src="img/layer/moveup.png" width=50 height=50/>\
Reorder the layer, move it up by one depth.

### Color
This is an HSV color picker. You can also enter a desired RGB value.
The color picked here will be used across all tools like brush, circle, text, ...

### Tool
This is a setting of tools. You have to first select a tool to see its options. The available options are listed below:

#### Brush
- Size
- Opacity
- Brush: 3 Shapes (Circle, Square, Triangle)

#### Eraser
- Size
- Brush: 3 Shapes (Circle, Square, Triangle)
#### Line
- Width
- Opacity
#### Cicle
- Width
- Opacity
- Filled: wheter the circle is filled, or only draw edges.
#### Square
- Width
- Opacity
- Filled: wheter the sqaure is filled, or only draw edges.
#### Triangle
- Width
- Opacity
- Filled: wheter the triangle is filled, or only draw edges.
#### Text
- Font: Choose among several fonts like `Arial`, `Times New Roman`, ...
- Size
- Opacity
- Bold
- Italic
- Text: Enter the text here, and use mouse clicks to paste it on the canvas.
#### Scale
- Proportional: whether the scaling should keep the aspect ratio, or can have x- and y-axis of different zomming factors.

### Quote
Whenever you are tired or feeling stressed out, the quotes can give you some inspiring, encouraging, and positive powers. Click on the tab, and you can get some random quotes.

